//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by Simon Ng on 11/8/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {

    let dataSource = RestaurantDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = dataSource
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Create an option menu as an action sheet
        let optionMenu = UIAlertController(title: nil, message: "What do you want to do?", preferredStyle: .actionSheet)

        // Add Cancel action to the menu
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        optionMenu.addAction(cancelAction)

        // Add Call action to the menu
        let callActionHandler = { (action: UIAlertAction!) -> Void in

            let alertMessage = UIAlertController(title: "Service Unavailable", message: "Sorry, the call feature is not available yet. Please retry later.", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)

        }

        let callAction = UIAlertAction(title: "Call " + "123-000-\(indexPath.row)", style: .default, handler: callActionHandler)
        optionMenu.addAction(callAction)

        // Add I've been here action to the menu
        let isVisitedTitle = dataSource.isVisitedTitle(row: indexPath.row)
        let isVisitedAction = UIAlertAction(title: isVisitedTitle, style: .default, handler: { (_: UIAlertAction) -> Void in

            let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell

            // Toggle checkmark
            self.dataSource.toggleVisited(indexPath.row)
            cell?.favorIconImageView.isHidden = !self.dataSource.isVisited(indexPath.row)
        })
        optionMenu.addAction(isVisitedAction)

        // Display the menu
        self.present(optionMenu, animated: true, completion: nil)

        // Deselect the cell
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.dataSource.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction] {

        let shareAction = UITableViewRowAction(style: .default, title: "Share", handler: { (_: UITableViewRowAction, _: IndexPath) -> Void in

            let shareMenu = UIAlertController(title: nil, message: "Share using", preferredStyle: .actionSheet)
            let twitterAction = UIAlertAction(title: "Twitter", style: .default, handler: nil)
            let facebookAction = UIAlertAction(title: "Facebook", style: .default, handler: nil)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

            shareMenu.addAction(twitterAction)
            shareMenu.addAction(facebookAction)
            shareMenu.addAction(cancelAction)

            self.present(shareMenu, animated: true, completion: nil)
        }
        )

        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete", handler: { (_: UITableViewRowAction, indexPath: IndexPath) -> Void in

            // Delete the row from the data source
            self.dataSource.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)

        }
        )

        shareAction.backgroundColor = UIColor(red: 255.0/255.0, green: 166.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        deleteAction.backgroundColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)

        return [deleteAction, shareAction]
    }

}

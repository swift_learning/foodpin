//
//  Restaurant.swift
//  FoodPin
//
//  Created by iulian david on 04/10/2019.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit

class Restaurant {

    let name: String
    let imageName: String
    let location: String
    let type: String
    var isVisited: Bool = false

    var image: UIImage {
        guard let image = UIImage(named: imageName) else {
            fatalError("Incorrect configuration")
        }
        return image
    }

    init(name: String, imageName: String, location: String, type: String) {
        self.name = name
        self.imageName = imageName
        self.location = location
        self.type = type
    }
}
